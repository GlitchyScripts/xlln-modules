#pragma once
#include "../resource.h"

#define DLL_TITLE               STRINGIZE(PROJECT_NAME)
#define DLL_DESCRIPTION         "An XLiveLessNess module for Halo 2 For Windows Vista."
#define DLL_AUTHOR              "Glitchy Scripts, NukeULater, num0005"
#define DLL_NAME                STRINGIZE(PROJECT_NAME) ".dll"
#define DLL_COPYRIGHT_YEAR      STRINGIZE(BUILD_DATETIME_YEAR)
#define DLL_VERSION_MAJOR       1
#define DLL_VERSION_MINOR       2
#define DLL_VERSION_REVISION    1
#define DLL_VERSION_BUILD       1
