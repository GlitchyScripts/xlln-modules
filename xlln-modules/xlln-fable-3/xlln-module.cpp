#include "../dllmain.hpp"
#include "xlln-module.hpp"
#include "../xlivelessness.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-hook.hpp"
#include "../utils/util-checksum.hpp"
#include <tchar.h>
#include <vector>

TITLE_VERSION::TYPE title_version = TITLE_VERSION::tUNKNOWN;

static uint32_t GetOffsetAddress(TITLE_VERSION::TYPE version, uint32_t offset)
{
	if (offset == INVALID_OFFSET || (title_version != version && version != TITLEMODULEANY)) {
		return 0;
	}
	return (uint32_t)xlln_hmod_title + offset;
}

static bool xmod_log_funcs_patched = false;
static size_t xmod_file_offset_to_rva = 0;

static const size_t xmod_log_func_type_130_offset = 0x01153549;
static std::vector<uint8_t*> xmod_log_func_type_130;

static const size_t xmod_log_func_type_131_offset = 0x0115354a;
static std::vector<uint8_t*> xmod_log_func_type_131;

static const size_t xmod_log_func_type_132_offset = 0x01153551;
static std::vector<uint8_t*> xmod_log_func_type_132;

static void __cdecl LogFuncType130(uint32_t log_level, const char* const message_format, ...)
{
	va_list message_arguments;
	va_start(message_arguments, message_format);
	int messageLength = _vscprintf_p(message_format, message_arguments);
	
	if (messageLength < 0) {
		va_end(message_arguments);
		__debugbreak();
		return;
	}
	
	size_t messageSizeLength = messageLength + 1;
	char* message = new char[messageSizeLength];
	messageLength = _vsnprintf_s(message, messageSizeLength, _TRUNCATE, message_format, message_arguments);
	va_end(message_arguments);
	
	if (messageLength < 0) {
		delete[] message;
		message = 0;
		__debugbreak();
		return;
	}
	
	uint32_t newLogLevel = XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO;
	
	if (
		message[0] != 0
		&& !(message[0] == ' ' && message[1] == 0)
		&& !(message[0] == 'v' && message[1] == 'o' && message[2] == 'i' && message[3] == 'd' && message[4] == 0)
	) {
		XLLN_DEBUG_LOG(newLogLevel, "130 0x%x: %s", log_level, message);
	}
	
	delete[] message;
	message = 0;
}

static void __cdecl LogFuncType131(const char* const message_format, ...)
{
	va_list message_arguments;
	va_start(message_arguments, message_format);
	int messageLength = _vscprintf_p(message_format, message_arguments);
	
	if (messageLength < 0) {
		va_end(message_arguments);
		__debugbreak();
		return;
	}
	
	size_t messageSizeLength = messageLength + 1;
	char* message = new char[messageSizeLength];
	messageLength = _vsnprintf_s(message, messageSizeLength, _TRUNCATE, message_format, message_arguments);
	va_end(message_arguments);
	
	if (messageLength < 0) {
		delete[] message;
		message = 0;
		__debugbreak();
		return;
	}
	
	uint32_t newLogLevel = XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO;
	
	if (
		message[0] != 0
		&& !(message[0] == ' ' && message[1] == 0)
	) {
		XLLN_DEBUG_LOG(newLogLevel, "131: %s", message);
	}
	
	delete[] message;
	message = 0;
}

static void __cdecl LogFuncType132(uint32_t log_level, const char* const message_format, ...)
{
	va_list message_arguments;
	va_start(message_arguments, message_format);
	int messageLength = _vscprintf_p(message_format, message_arguments);
	
	if (messageLength < 0) {
		va_end(message_arguments);
		__debugbreak();
		return;
	}
	
	size_t messageSizeLength = messageLength + 1;
	char* message = new char[messageSizeLength];
	messageLength = _vsnprintf_s(message, messageSizeLength, _TRUNCATE, message_format, message_arguments);
	va_end(message_arguments);
	
	if (messageLength < 0) {
		delete[] message;
		message = 0;
		__debugbreak();
		return;
	}
	
	uint32_t newLogLevel = XLLN_LOG_CONTEXT_TITLE | XLLN_LOG_LEVEL_INFO;
	
	if (
		message[0] != 0
	) {
		XLLN_DEBUG_LOG(newLogLevel, "132 0x%x: %s", log_level, message);
	}
	
	delete[] message;
	message = 0;
}

static bool FindLogCalls()
{
	IMAGE_DOS_HEADER* imageDosHeader = (IMAGE_DOS_HEADER*)xlln_hmod_title;
	
	if (imageDosHeader->e_magic != IMAGE_DOS_SIGNATURE) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_ERROR
			, "[XLLN-Fable-3] %s unable to find the loaded image's DOS header signature."
			, __func__
		);
		
		return false;
	}
	
	IMAGE_NT_HEADERS* imageNtHeaders = (IMAGE_NT_HEADERS*)((size_t)xlln_hmod_title + imageDosHeader->e_lfanew);
	
	if (imageNtHeaders->Signature != IMAGE_NT_SIGNATURE) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_ERROR
			, "[XLLN-Fable-3] %s unable to find the loaded image's NT header signature."
			, __func__
		);
		return false;
	}
	
	uint16_t imageSectionHeaderTableSectionCount = imageNtHeaders->FileHeader.NumberOfSections;
	IMAGE_SECTION_HEADER* imageSectionHeaderTable = (IMAGE_SECTION_HEADER*)((size_t)xlln_hmod_title + imageDosHeader->e_lfanew + sizeof(IMAGE_NT_HEADERS));
	
	IMAGE_SECTION_HEADER* imageSectionHeaderText = 0;
	
	for (uint16_t iSection = 0; iSection < imageSectionHeaderTableSectionCount; iSection++) {
		if (strncmp((char*)imageSectionHeaderTable[iSection].Name, ".text", IMAGE_SIZEOF_SHORT_NAME) == 0) {
			imageSectionHeaderText = &imageSectionHeaderTable[iSection];
		}
	}
	
	if (!imageSectionHeaderText) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLLN_MODULE | XLLN_LOG_LEVEL_ERROR
			, "[XLLN-Fable-3] %s unable to find '.text' section header in binary file."
			, __func__
		);
		return false;
	}
	
	xmod_file_offset_to_rva = imageSectionHeaderText->VirtualAddress - imageSectionHeaderText->PointerToRawData;
	
	uint8_t patternSearch[] = {0xe8};
	size_t patternSearchSize = sizeof(patternSearch);
	
	const uint8_t* patternMatch = (const uint8_t*)((size_t)xlln_hmod_title + imageSectionHeaderText->VirtualAddress);
	size_t remainingSize = (size_t)imageSectionHeaderText->SizeOfRawData;
	while (1) {
		patternMatch = FindBytes(patternMatch, remainingSize, (const uint8_t*)patternSearch, patternSearchSize, true);
		if (!patternMatch) {
			break;
		}
		remainingSize = (size_t)imageSectionHeaderText->SizeOfRawData - ((size_t)patternMatch - ((size_t)xlln_hmod_title + imageSectionHeaderText->VirtualAddress));
		
		if (remainingSize < 5) {
			break;
		}
		
		size_t patternCallAddress = ((size_t)(&patternMatch[5]) + *(int32_t*)(&patternMatch[1]));
		size_t patternCallFileOffset = patternCallAddress - ((size_t)xlln_hmod_title + xmod_file_offset_to_rva);
		switch (patternCallFileOffset) {
			case xmod_log_func_type_130_offset: {
				xmod_log_func_type_130.push_back((uint8_t*)patternMatch);
				break;
			}
			case xmod_log_func_type_131_offset: {
				xmod_log_func_type_131.push_back((uint8_t*)patternMatch);
				break;
			}
			case xmod_log_func_type_132_offset: {
				xmod_log_func_type_132.push_back((uint8_t*)patternMatch);
				break;
			}
		}
		
		patternMatch += 1;
		remainingSize += 1;
	}
	
	return true;
}

static uint32_t AddPatches()
{
	uint32_t patchAddress = 0;
	
	xmod_log_funcs_patched = FindLogCalls();
	if (xmod_log_funcs_patched) {
		for (uint8_t* callInstruction : xmod_log_func_type_130) {
			PatchCall(callInstruction, LogFuncType130);
		}
		for (uint8_t* callInstruction : xmod_log_func_type_131) {
			PatchCall(callInstruction, LogFuncType131);
		}
		for (uint8_t* callInstruction : xmod_log_func_type_132) {
			PatchCall(callInstruction, LogFuncType132);
		}
	}
	
	return ERROR_SUCCESS;
}

static uint32_t RemovePatches()
{
	uint32_t patchAddress = 0;
	
	if (xmod_log_funcs_patched) {
		for (uint8_t* callInstruction : xmod_log_func_type_130) {
			PatchCall(callInstruction, (uint8_t*)((size_t)xlln_hmod_title + xmod_file_offset_to_rva + xmod_log_func_type_130_offset));
		}
		for (uint8_t* callInstruction : xmod_log_func_type_131) {
			PatchCall(callInstruction, (uint8_t*)((size_t)xlln_hmod_title + xmod_file_offset_to_rva + xmod_log_func_type_131_offset));
		}
		for (uint8_t* callInstruction : xmod_log_func_type_132) {
			PatchCall(callInstruction, (uint8_t*)((size_t)xlln_hmod_title + xmod_file_offset_to_rva + xmod_log_func_type_132_offset));
		}
	}
	
	return ERROR_SUCCESS;
}

// #41101
uint32_t WINAPI XLLNModulePostInit()
{
	uint32_t result = ERROR_SUCCESS;
	
	AddPatches();
	
	return result;
}

// #41102
uint32_t WINAPI XLLNModulePreUninit()
{
	uint32_t result = ERROR_SUCCESS;
	
	RemovePatches();
	
	return result;
}

bool InitXllnModule()
{
	{
		char *checksumTitle = GetPESha256FlagFix(xlln_hmod_title);
		if (!checksumTitle) {
			return false;
		}
		
		if (_strcmpi(checksumTitle, "5a6dc0f02aae4fb2fd7b67662f3c9a65a6f1c446efb13d661f01730e0e01c914") == 0) {
			title_version = TITLE_VERSION::tTITLE_SHIPPED;
		}
		
		free(checksumTitle);
		
		if (title_version == TITLE_VERSION::tUNKNOWN) {
			return false;
		}
	}
	
	return true;
}

bool UninitXllnModule()
{
	return true;
}
